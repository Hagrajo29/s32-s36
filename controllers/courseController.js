const User = require("../models/user.js")
const Course = require("../models/course.js")

module.exports.addCourse = (reqBody,userData) =>{
	return User.findById(userData.userId).then(result => {
		if(userData.isAdmin === true) {
			let newCourse = new Course({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			})
			return newCourse.save().then((course, error) =>{
				if (error){
					console.log(error)
					return false
				}else{
					return "Course creation successful.";
				}
			})
		} else {
			return 'You are not an Admin.';
		}
	})
}

// retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find( {} ).then((result, error) => {
		if (error) {
			return false
		}else{
			return result
		}
	})
}



module.exports.getCourse = (reqParams) => {
	console.log(reqParams.courseId)
	return Course.findById(reqParams.courseId).then(result=> {
			return result
	})
}


module.exports.getActive = () => {
	return Course.find({isActive:true}).then((result,error)=> {
		if (error) {
			console.log(error)
		}else{
			return result
		}
	})
}



module.exports.updateCourse = (reqParams,reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price:reqBody.price
	}
	//findByIdandUpdate - looks for the id of the document(first parameter) and updates it (content of the second parameter)
	//the server will be looking for the id of the reqParams.courseId in the database and updates the document through the content of the object updatedCourse 
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

/*
routes business logic:

 use /archiveCourse and send a PUT request to archive a course by changing the active status
controllers business logic: 

1. create a updateCourse object with the content from the reqBody
	reqBody should have the isActive status of the course to be set to false
 2. find the course by its id and update with the updateCourse object using the findByIdAndUpdate
	 handle the errors that may arise
		error/s - false
		no errors - true

*/
module.exports.archivedCourse = (reqParams,reqBody) => {
	let updatedCourse = {
		isActive: reqBody.isActive
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error) => {
		if (error) {
			return false
		} else	{
			return true
		}
	})
}

/*module.exports.enrollees = async (data) => {
	let isEnrolleesUpdated = await Course.findById(data.courseId).then(course => {
		course.enrollees.push( {_id: data.userId} )
		return course.save().then((course, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})

	if (isEnrolleesUpdated){
		return true
	}
	else{
		return false
	}
}
*/