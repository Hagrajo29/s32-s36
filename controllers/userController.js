//set up dependencies

const User = require("../models/user.js")
const Course = require("../models/course.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt") //used to encrypt user passwords


// check if the email exists
/*
	1. check for the email in the database
	2. send the result as a response (with error handling)
*/
/*
	it is conventional for the devs to use Boolean in sending return responses esp with the backend application
*/
module.exports.checkEmail = (requestBody) => {
	return User.find({ email: requestBody.email }).then((result, error) => {
		if (error) {
			console.log(error)
			return false
		}else{
			if (result.length > 0) {
				// return result
				return true
			} else {
				// return res.send("email does not exist")
				return false
			}
		}
	})
}

//USER REGISTRATION

module.exports.registerUser = (reqBody) =>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		age: reqBody.age,
		gender: reqBody.gender,
		email: reqBody.email,
		// hashSync is a function of bcrypt that encrypts the password
			// 10 the number of rounds/times it runs the algorithm to the reqBody.password
				// max 72 implementations
		password: bcrypt.hashSync(reqBody.password, 10),
		mobileNo: reqBody.mobileNo
	})
	return newUser.save().then((saved, error) =>{
		if (error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

// USER LOGIN


module.exports.userLogin = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			// compareSync function - used to compare a non-encrypted password to an encrypted password and retuns a Boolean reasponse depending on the result
			/*
			What should we do after the comparison?
				true - a token should be created since the user is existing and the password is correct
				false - the passwords do not match, thus a toke should not be created
			*/
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
				// auth - imported auth.js
				// createAccessToken - function inside the auth.js to create access token
				// .toObject - transforms the User into an Object that can be used by our createAccessToken
			} else {
				return false
			}
		}
	})
}


//Get Profile with bearer token(access token)
	
module.exports.getProfile = (data) => {
	// find the user by the id
	return User.findById( data.userId ).then(result =>{
		// if there is no user found...
		if (result === null) {
			return false
		// if a user is found...
		} else {
			result.password = "";
			return result
		}
	})
}

/*
module.exports.enrollUser = (requestBody)=>{
	return User.findById( data.courseId ).then(result =>{
		if (result === null) {
			return false
		} else {
			return result
		}
	})
}
*/

module.exports.enroll = async (data) => {
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// adding the courseId to the user's enrollment array
		user.enrollments.push( {courseId: data.courseId} )

		// saving in the database
		return user.save().then((user, err) => {
			if(err){
				return false
			} else {
				return true
			}
		})
	})
	let isCourseUpdated = await Course.findById(data.courseId).then(course => {
		// adding userId in the course enrollees array
		course.enrollees.push({userId: data.userId})
		// saving to database
		return course.save().then((course, error)=>{
			if(error){
				return false
			}else{
				return true
			}
		})
	})
	if (isUserUpdated && isCourseUpdated){
		return true
	}
	else{
		return false
	}
}





