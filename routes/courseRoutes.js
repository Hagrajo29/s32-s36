const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const courseController = require("../controllers/courseController.js")


router.post("/addCourse", auth.verify, ( req,res) => {
	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController));
})


router.get("/", (req,res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController))
})

router.get("/active", (req,res) => {
	courseController.getActive().then(resultFromController => res.send(resultFromController))
})

router.get("/:courseId", (req,res) =>{
	console.log(req.params.courseId);
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController))
})


router.put("/:courseId", auth.verify,(req,res) => {
	courseController.updateCourse(req.params,req.body).then(result => res.send(result))
})

router.put("/:courseId/archive", auth.verify,(req,res) => {
	courseController.archivedCourse(req.params,req.body).then(result => res.send(result))
})

/*router.post("/updateEnrollees",(req,res) =>{
	let data = {
		courseId:req.body.courseId,
		userId: req.body.userId
	}
	courseController.enrollees(data).then(result => res.send(result))
})*/



module.exports = router;